## Desafio Desenvolvedor Mobile Android
Iplanrio - Empresa Municipal de Informática do RJ

### **Avaliação**
Esta etapa do processo seletivo consiste no desenvolvimento de um aplicativo Android, com a finalidade de avaliar as habilidades dos candidatos de vários níveis.

#### **Seu projeto será avaliado de acordo com os seguintes critérios:**
- Sua aplicação preenche os requerimentos básicos?
- Você seguiu as instruções de envio do desafio?

### **Prazo de Entrega**
Após o recebimento do desafio, pedimos que a atividade seja realizada dentro do período de 3 dias úteis.

### **Instruções de Entrega**
- O candidato deverá criar um fork do repositório no Bitbucket;

### **Requisitos**
O candidato deve criar um aplicativo para o sistema operacional Android com utilização dos recursos de Mapa.

##### **Parte 1 - Home:**
- O aplicativo deve exibir inicialmente uma tela com:
  - o mapa carregado;
  - a localização do usuário deve ser marcada por um Pin;
  - campo de texto para informar o endereço de origem;
  - campo de origem preenchido com a localização do usuário.
  - campo de texto para informar o endereço de destino;
  - botão "Salvar";
  
- Traçar Rota:
  - Ao preencher o campo de destino deverá ser traçado uma rota entre a origem e o destino.

- Clicar botão "Salvar":
    - armazenar em um banco de dados local as informações da rota traçada;


##### **Parte 2 - Histórico de Corridas:**
  - Exibir tela de histórico de corridas contendo em cada registro:
    * origem;
    * destino;
    * data da solicitação;
    * distância;
    * tempo estimado da corrida.


### **Requisitos técnicos obrigatórios**
- desenvolver com a linguagem de programação Kotlin;
- suportar o AndroidX;
- **Na parte 2 (Histórico de Corridas)** - Consumir um serviço de mock para simular acesso a API. Ex: apiary ou qualquer outro de sua preferência.
- usar API do Google Maps: Maps SDK para Mapa, Geocoder, Directions (Rota);

### **Requisitos técnicos desejáveis**
- usar um arquivo .gitignore no seu repositório;
- utilizar algum Componente do Android Jetpack. Ex: Room, Paging, WorkManager.
- criar testes unitários.
- criar testes funcionais.
